import { VendorsComponent } from './vendors/vendors.component';
import { ProductsComponent } from './products/products.component';
import { CustomersComponent } from './customers/customers.component';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path : '', component : HomeComponent, pathMatch : 'full' },
  { path : 'home', component : HomeComponent, pathMatch : 'full' },
  { path : 'customers', component : CustomersComponent, pathMatch : 'full' },
  { path : 'products', component : ProductsComponent, pathMatch : 'full' },
  { path : 'vendors', component : VendorsComponent, pathMatch : 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
