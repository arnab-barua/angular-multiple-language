import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private translate : TranslateService) {
    
    translate.get('demo').subscribe(texts => console.log(texts));
  }

  ngOnInit() {
  }

  useLanguage(lan) {
    this.translate.use(lan);
    console.log(this.translate.instant('demo.greeting', {'name' : 'sazzad', 'company' : 'google'}))
    // this.translate.get('demo.greeting', {'name' : 'sazzad', 'company' : 'google'}).subscribe(texts => console.log(texts));
  }

}
